//
//  HomeModel.swift
//  22
//
//  Created by VanjaPin on 22.05.2020.
//  Copyright © 2020 VanjaPin. All rights reserved.
//


import Foundation

class Workout {
    var id: Int = 0
    var name: String = ""
    var exersice:[Exersice] = []
}
class Exersice {
    var id: Int = 0
    var isOpen = Bool()
    var title = String()
    var unit1 = String()
    var unit2 = String()
    var task : [DailyTask] = []
}
class DailyTask {
    var date = String()
    var values: [IndividualTask] = []
}
class IndividualTask {
    var value1 = Int()
    var value2 = Int()
    
}
class HomeModel {
    var workout: [Workout] = []
}

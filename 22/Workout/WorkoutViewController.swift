//
//  WorkoutViewController.swift
//  MVPSportNote
//
//  Created by Andrew Peneznyk on 20.05.2020.
//  Copyright © 2020 Andrew Penzenyk. All rights reserved.
//

import UIKit

protocol  WorkoutDisplayLogic: NSObjectProtocol {
    
}


class WorkoutViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var addButton: UIView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    var presenter: WorkoutBusinessLogic = WorkoutPresenter()
    var data: WorkoutModel = WorkoutModel()
    var isEdit = false;
    var previousExpand = -1
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = presenter.getTitle()
        tableView?.dataSource = self
        tableView?.delegate = self
        addButton.layer.cornerRadius = addButton.frame.height/2
        emptyView.isHidden = data.workout.exersice.count>0
        tableView.register(UINib(nibName: "WorkoutHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "WorkoutHeaderTableViewCell")
        tableView.register(UINib(nibName: "WorkoutTableViewCell", bundle: nil), forCellReuseIdentifier: "WorkoutTableViewCell")
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
    }
    @IBAction func EditTapped(_ sender: Any) {
        isEdit = !isEdit
        //editButton.image =  UIImage(systemName: isEdit ?"checkmark.circle": "pencil.circle")
        addButton.isHidden = isEdit
        tableView.setEditing(isEdit, animated: true)
        tableView.reloadData()
    }
    
    @IBAction func AddTapped(_ sender: Any) {
        data = presenter.AddExersice()
        reload()
    }
    func reload(){
        emptyView.isHidden = data.workout.exersice.count>0
        tableView.reloadData()
    }
    
}
extension WorkoutViewController: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if data.workout.exersice[section].isOpen  {
            return 2// data.workout.exersice[section].task.count
        }
        else
        {
            return 1
        }
        //return  2//data.workout.exersice.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "WorkoutHeaderTableViewCell", for: indexPath) as! WorkoutHeaderTableViewCell
            cell.setupCell(index: indexPath.section, data: data.workout.exersice[indexPath.section],state: isEdit)
            cell.cellDelegate = self
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "WorkoutTableViewCell", for: indexPath) as! WorkoutTableViewCell
            cell.setupCell(index: indexPath.section, data: data.workout.exersice[indexPath.section],state: isEdit)
            return cell
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.workout.exersice.count
        //return 2
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        data = presenter.SwapExersice(source: sourceIndexPath.row, destination: destinationIndexPath.row)
        reload()
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.none
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

           if previousExpand != -1 && previousExpand != indexPath.section
           {
                data.workout.exersice[previousExpand].isOpen = false
                let sections1 = IndexSet.init(integer: previousExpand)
                tableView.reloadSections(sections1, with: .none)
           }
        
        data.workout.exersice[indexPath.section].isOpen = !data.workout.exersice[indexPath.section].isOpen
        let sections = IndexSet.init(integer: indexPath.section)
        tableView.reloadSections(sections, with: .none)
        previousExpand = indexPath.section
        // tableView.deselectRow(at: indexPath, animated: true)
        //  performSegue(withIdentifier: "WorkoutSegue", sender: self)
    }
    
}
extension WorkoutViewController:HomeTableViewCellProtocol{
    func OnXClick(index: Int) {
       
            data = presenter.DeleteExersice(source: index)
            isEdit = data.workout.exersice.count != 0
            //editButton.image =  UIImage(systemName: isEdit ?"checkmark.circle": "pencil.circle")
            addButton.isHidden = isEdit
            tableView.setEditing(isEdit, animated: true)
            tableView.reloadData()
            reload()
        
    }
    func NameChanged(index: Int, newName: String){
        
    }
    
}

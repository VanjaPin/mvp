//
//  WorkoutTableViewCell.swift
//  22
//
//  Created by VanjaPin on 22.05.2020.
//  Copyright © 2020 VanjaPin. All rights reserved.
//

import UIKit



class WorkoutTableViewCell: UITableViewCell {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var backgroundImage: UIImageView!
    var data:Exersice = Exersice()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    
    func setupCell(index: Int,data: Exersice,state: Bool){
        self.data = data
        stackView.removeFullyAllArrangedSubviews()
        backgroundImage.backgroundColor = getColor()
        
        
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 8.0
        
        
        stackView.addArrangedSubview(addTask())
        stackView.addArrangedSubview(addTask())
        stackView.addArrangedSubview(addTask())
        stackView.addArrangedSubview(addTask())
        stackView.addArrangedSubview(addTask())
        stackView.addArrangedSubview(addTask())
        stackView.addArrangedSubview(addButton())
        
        
        
        
    }
    private func addTask()-> UIView{
        let view = UIView()
        let labelDate = UILabel()
        labelDate.text = "Today"
        labelDate.tintColor = UIColor.white
        labelDate.textAlignment  = .center
        view.addSubview(labelDate)
        labelDate.translatesAutoresizingMaskIntoConstraints = false
        labelDate.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        labelDate.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        let labelDescription = UILabel()
        labelDescription.text = "0 x 0"
        labelDescription.tintColor = UIColor.black
        labelDescription.textAlignment  = .center
        view.addSubview(labelDescription)
        labelDescription.translatesAutoresizingMaskIntoConstraints = false
        labelDescription.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        labelDescription.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
        return view
    }
    
    private func addButton()-> UIView{
        let view = UIView()
        
        let button = UIButton()
        button.setTitle("", for: .normal)
        //button.setImage(UIImage(systemName: "plus"), for: .normal)
        button.tintColor = UIColor.white
        button.backgroundColor = getColor()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant:  30).isActive = true
        button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        button.layer.cornerRadius = 15
        view.addSubview(button)
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button.topAnchor.constraint(equalTo: view.topAnchor, constant: 30).isActive = true
        return view
    }
    
    private func getColor()->UIColor{
        var color: UIColor
        switch (data.id-1) % 3 {
        case 0:
            color = UIColor.blue
        case 1:
            color = UIColor.red
        case 2:
            color = UIColor.green
        default:
            color = UIColor.blue
        }
        return color
    }
}


extension UIStackView {
    
    func removeFully(view: UIView) {
        removeArrangedSubview(view)
        view.removeFromSuperview()
    }
    
    func removeFullyAllArrangedSubviews() {
        arrangedSubviews.forEach { (view) in
            removeFully(view: view)
        }
    }
    
}
